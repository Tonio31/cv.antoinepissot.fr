import { TestBed } from '@angular/core/testing';

import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { LoggerService } from '@app/_services/logger.service';
import { MockLoggerService } from '@app/_services/logger.service.spec';

describe( 'HttpService', () => {


  class MockHttpClient {
    public get() {
      return of( {
        status: 200,
        body: {
          message: 'ok'
        },
      } );
    }
    public post() {
      return of( {
        status: 200,
        body: {
          message: 'ok'
        },
      } );
    }
  }


  let service: HttpService;
  let httpClient: HttpClient;
  beforeEach( () => {
    TestBed.configureTestingModule( {
      providers: [
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: LoggerService, useClass: MockLoggerService },
      ]
    } );

    service = TestBed.get( HttpService );
    httpClient = TestBed.get( HttpClient );
  } );

  it( 'sendMessage()', () => {
    spyOn( service, 'postQuery' );
    service.sendMessage( {
      subject: 'something',
      email: 'patrice@mandela.com',
      message: 'contact me a soon as you can'
    } );
    expect( service.postQuery ).toHaveBeenCalled();
  } );

  it( 'getQuery()', () => {
    spyOn( httpClient, 'get' ).and.callThrough();
    const resp = service.getQuery( 'https://cdn.potentialife.com/', 'blob' );

    resp.subscribe( ( response ) => {
      expect( response ).toEqual( {
        status: 200,
        response: {
          message: 'ok'
        },
      } );
    } );
  } );

  it( 'getBlobQuery()', () => {
    spyOn( service, 'getQuery' );
    service.getBlobQuery( 'https://cdn.potentialife.com/' );
    expect( service.getQuery ).toHaveBeenCalledWith( 'https://cdn.potentialife.com/', 'blob' );
  } );

  it( 'postQuery()', () => {
    spyOn( httpClient, 'get' ).and.callThrough();
    const resp = service.postQuery( 'https://cdn.potentialife.com/', );

    resp.subscribe( ( response ) => {
      expect( response ).toEqual( {
        status: 200,
        response: {
          message: 'ok'
        },
      } );
    } );
  } );

} );
