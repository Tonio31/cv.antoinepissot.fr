/* tslint:disable no-console */

import { Injectable } from '@angular/core';

export interface LogHistory {
  type: string;
  log: string;
}

export abstract class Logger {
  log: any;
  info: any;
  warn: any;
  error: any;
}

const noop = (): any => undefined;

@Injectable(
  {
    providedIn: 'root',
  },
)
export class LoggerService implements Logger {

  public logHistory: LogHistory[] = [];

  public readonly LOG_TYPE = {
    log: 'log',
    info: 'info',
    warn: 'warn',
    error: 'error',
  };

  constructor() {
  }

  public getLogHistory() {
    return this.logHistory;
  }

  public addLog( iLogType: string, iLogLine: string ) {
    this.logHistory.push( {
      type: iLogType,
      log: iLogLine
    } );
  }

  public log( ...args ) {
    console.log( ...args );
    this.addLog( this.LOG_TYPE.log, JSON.stringify( args ) );
  }

  public info( ...args ) {
    console.info( ...args );
    this.addLog( this.LOG_TYPE.info, JSON.stringify( args ) );
  }

  public warn( ...args ) {
    console.warn( ...args );
    this.addLog( this.LOG_TYPE.warn, JSON.stringify( args ) );
  }

  public error( ...args ) {
    console.error( ...args );
    this.addLog( this.LOG_TYPE.error, JSON.stringify( args ) );
  }

}
