import { Injectable } from '@angular/core';
import { WindowRefService } from '@app/_services/window-ref.service';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private readonly KEY_LOCAL_STORAGE = 'cv.antoinepissot.fr';

  public storage: any;

  private localStorageObject: any = {};

  constructor( private winRef: WindowRefService ) {
    // We do this for unit testing, so we can mock localStorage
    this.storage = this.winRef.nativeWindow.localStorage;

    // if nothing exist in local storage create an empty object
    if ( ! JSON.parse( this.storage.getItem( this.KEY_LOCAL_STORAGE ) ) ) {
      this.storage.setItem( this.KEY_LOCAL_STORAGE, JSON.stringify( {} ) );
    }
  }

  public setToken( iToken: string ) {
    this.localStorageObject.token = iToken;
    this.storage.setItem( this.KEY_LOCAL_STORAGE, JSON.stringify( this.localStorageObject ) );
  }

  public getToken() {
    if ( !this.localStorageObject.token ) {
      this.localStorageObject.token = JSON.parse( this.storage.getItem( this.KEY_LOCAL_STORAGE ) ).token;
    }

    return this.localStorageObject.token;
  }
}
