import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactComponent, STATUS_SEND_MESSAGE } from './contact.component';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpService } from '@app/_services/http.service';
import { LoggerService } from '@app/_services/logger.service';
import { MockLoggerService } from '@app/_services/logger.service.spec';
import { of } from 'rxjs';

describe( 'ContactComponent', () => {

  @Pipe( { name: 'translate' } )
  class MockPipe implements PipeTransform {
    public transform( value: number ): number {
      return value;
    }
  }

  class MockHttpService {
    public sendMessage() {
      return of( {} );
    }
  }

  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;

  let http: HttpService;

  beforeEach( async( () => {
    TestBed.configureTestingModule( {
      declarations: [
        ContactComponent,
        MockPipe
      ],
      imports: [ ReactiveFormsModule ],
      providers: [
        { provide: HttpService, useClass: MockHttpService },
        { provide: LoggerService, useClass: MockLoggerService },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    } )
    .compileComponents();

  } ) );

  beforeEach( () => {
    fixture = TestBed.createComponent( ContactComponent );
    component = fixture.componentInstance;

    http = TestBed.get( HttpService );

    fixture.detectChanges();

    jasmine.clock().uninstall();
    jasmine.clock().install();


    const baseTimeUTC = new Date( Date.UTC( 2018, 9, 8 ) );
    jasmine.clock().mockDate( baseTimeUTC );

  } );

  afterEach( () => {
    jasmine.clock().uninstall();
  } );


  it( 'should create', () => {
    expect( component ).toBeTruthy();
  } );

  it( 'onSubmit() - send a request to the backend to send an email', () => {
    spyOn( http, 'sendMessage' ).and.callThrough();
    spyOn( component.contactForm, 'reset' );

    component.contactForm.controls.email.setValue('valid@email.com' );
    component.contactForm.controls.subject.setValue('valid subject' );
    component.contactForm.controls.content.setValue('valid content' );

    component.onSubmit();

    expect( http.sendMessage ).toHaveBeenCalled();
    expect( component.contactForm.reset ).toHaveBeenCalled();
    expect( component.statusSendMessage ).toEqual( STATUS_SEND_MESSAGE.SUCCESS );

    // Trick to trigger what's inside setTimeout()
    jasmine.clock().tick( 10001 );

    expect( component.statusSendMessage ).toEqual( STATUS_SEND_MESSAGE.NOT_SENT );
  } );
} );
