import { Component, ElementRef, OnInit } from '@angular/core';
import { WindowRefService } from '@app/_services/window-ref.service';
import { LoggerService } from '@app/_services/logger.service';


import * as menuItem from '@app/reducers/menuItem/menuItem.reducer';

import * as fromRoot from '@app/reducers';
import { select, Store } from '@ngrx/store';
import { MenuItemType } from '@app/reducers/menuItem/menuItem.model';

@Component( {
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ],
} )
export class HomeComponent implements OnInit {

  constructor( private store: Store<menuItem.State>,
               private logger: LoggerService,
               private winRef: WindowRefService,
               private el: ElementRef ) {
  }

  public ngOnInit() {
    this.store.pipe( select( fromRoot.getMenuItemState ) ).subscribe( ( menuItemClicked ) => {
      this.logger.log( 'Home - updateState menuItemClicked=', menuItemClicked );
      if ( menuItemClicked ) {
        this.scrollToCategory( menuItemClicked.currentMenuItem );
      }
    });
  }

  public scrollToCategory( iMenuItemCategory ) {

    this.logger.log( 'Home - scrollToCategory iMenuItemCategory=', iMenuItemCategory );

    let cssSelector: string = null;
    if ( iMenuItemCategory === MenuItemType.PROFILE ) {
      cssSelector = 'app-profile';
    }
    else if ( iMenuItemCategory === MenuItemType.EXPERTISE ) {
      cssSelector = 'app-expertise';
    }
    else if ( iMenuItemCategory === MenuItemType.EXPERIENCE ) {
      cssSelector = 'app-experience';
    }
    else if ( iMenuItemCategory === MenuItemType.EDUCATION ) {
      cssSelector = 'app-education';
    }
    else if ( iMenuItemCategory === MenuItemType.LANGUAGES ) {
      cssSelector = 'app-languages';
    }
    else if ( iMenuItemCategory === MenuItemType.INTERESTS ) {
      cssSelector = 'app-interests';
    }
    else if ( iMenuItemCategory === MenuItemType.CONTACT ) {
      cssSelector = 'app-contact';
    }

    setTimeout( () => {
      const element: any = this.el.nativeElement.querySelector(cssSelector);

      if ( element ) {
        const topOfElement = element.offsetTop - 60; // 60 is the height of the navbar
        setTimeout( () => {
          this.winRef.nativeWindow.scroll({ top: topOfElement, behavior: 'smooth' });
        } );
      }
    }, 100);

  }
}
