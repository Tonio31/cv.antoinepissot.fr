import { Component, Inject, OnInit } from '@angular/core';
import { LoggerService } from '@app/_services/logger.service';
import { Store } from '@ngrx/store';
import * as menuItem from '@app/reducers/menuItem/menuItem.reducer';

import { MenuItemClicked } from '@app/reducers/menuItem/menuItem.actions';
import { MenuItemType } from '@app/reducers/menuItem/menuItem.model';

@Component( {
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: [ './layout.component.scss' ]
} )
export class LayoutComponent implements OnInit {

  constructor( private store: Store<menuItem.State>,
               private logger: LoggerService, ) {
  }

  public ngOnInit() {
    this.logger.log( 'LayoutComponent() - ngOnInit()' );
  }

  public handleMenuItemClicked( iMenuItemType: MenuItemType ) {
    this.logger.log( 'LayoutComponent() - handleMenuItemClicked iMenuItemType=', iMenuItemType );

    this.store.dispatch( new MenuItemClicked( iMenuItemType ) );
  }
}
