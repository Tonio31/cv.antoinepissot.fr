import { Component, OnInit } from '@angular/core';
import { LogoInfo, LogoType } from '@app/components/presentation/logo/logo.component';

@Component({
  selector: 'app-expertise',
  templateUrl: './expertise.component.html',
  styleUrls: ['./expertise.component.scss']
})
export class ExpertiseComponent implements OnInit {

  public readonly logos: LogoInfo[] = [
    {
      type: LogoType.Angular
    },
    {
      type: LogoType.TypeScript
    },
    {
      type: LogoType.AngularJS
    },
    {
      type: LogoType.Javascript
    },
    {
      type: LogoType.HTML
    },
    {
      type: LogoType.CSS
    },
    {
      type: LogoType.SCSS
    },
    {
      type: LogoType.SQL
    },
    {
      type: LogoType.MongoDB
    },
    {
      type: LogoType.UML
    },
    {
      type: LogoType.MacOS
    },
    {
      type: LogoType.Linux
    },
    {
      type: LogoType.AWS
    },
    {
      type: LogoType.PhpStorm
    },
    {
      type: LogoType.SequelPro
    },
    {
      type: LogoType.Jira
    },
    {
      type: LogoType.Confluence
    },
    {
      type: LogoType.Bitbucket
    },
    {
      type: LogoType.Git
    },
    {
      type: LogoType.GitHub
    },
  ];

  constructor() { }

  public ngOnInit() {
  }

}
