import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carrousel-photos',
  templateUrl: './carrousel-photos.component.html',
  styleUrls: ['./carrousel-photos.component.scss']
})
export class CarrouselPhotosComponent implements OnInit {

  public readonly carrousselImages = [
    {
      source: 'assets/images/travels/inle-lake.jpg',
      sourceBig: 'assets/images/travels/inle-lake-big.jpg',
      caption: {
        cssClass: '',
        title: 'Myanmar - Inle Lake'
      }
    },
    {
      source: 'assets/images/travels/yellowstone-7colors-lake.jpg',
      sourceBig: 'assets/images/travels/yellowstone-7colors-lake-big.jpg',
      caption: {
        cssClass: '',
        title: 'USA - Yellowstone - Grand Prismatic Spring'
      }
    },
    {
      source: 'assets/images/travels/Yellowstone-waterfall.jpg',
      sourceBig: 'assets/images/travels/Yellowstone-waterfall-big.jpg',
      caption: {
        cssClass: '',
        title: 'USA - Yellowstone - Lower Falls'
      }
    },
    {
      source: 'assets/images/travels/bryce-canyon.jpg',
      sourceBig: 'assets/images/travels/bryce-canyon-big.jpg',
      caption: {
        cssClass: '',
        title: 'USA - Bryce Canyon'
      }
    },
    {
      source: 'assets/images/travels/cozumel.jpg',
      sourceBig: 'assets/images/travels/cozumel-big.jpg',
      caption: {
        cssClass: '',
        title: 'Mexico - Cozumel Island'
      }
    },
    {
      source: 'assets/images/travels/machu-picchu.jpg',
      sourceBig: 'assets/images/travels/machu-picchu-big.jpg',
      caption: {
        cssClass: '',
        title: 'Peru - Machu Picchu'
      }
    },
    {
      source: 'assets/images/travels/san-blas.jpg',
      sourceBig: 'assets/images/travels/san-blas-big.jpg',
      caption: {
        cssClass: 'carousel-top',
        title: 'Panama - San Blas Islands'
      }
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
