import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MenuItemType } from '@app/reducers/menuItem/menuItem.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public readonly tonioLogoUrl: string = 'assets/images/tonio-logo.png';

  public readonly menuItems = [
    {
      type: MenuItemType.PROFILE,
      titleToTranslate: 'PROFILE.TITLE_MENU'
    },
    {
      type: MenuItemType.EXPERIENCE,
      titleToTranslate: 'EXPERIENCE.TITLE_MENU'
    },
    {
      type: MenuItemType.EXPERTISE,
      titleToTranslate: 'EXPERTISE.TITLE_MENU'
    },
    {
      type: MenuItemType.EDUCATION,
      titleToTranslate: 'EDUCATION.TITLE_MENU'
    },
    {
      type: MenuItemType.LANGUAGES,
      titleToTranslate: 'LANGUAGES.TITLE_MENU'
    },
    {
      type: MenuItemType.INTERESTS,
      titleToTranslate: 'INTERESTS.TITLE_MENU'
    },
    {
      type: MenuItemType.CONTACT,
      titleToTranslate: 'CONTACT.TITLE_MENU'
    }
  ];


  @Output() onMenuItemClicked: EventEmitter<MenuItemType>;

  constructor() {
    this.onMenuItemClicked = new EventEmitter<MenuItemType>();
  }

  public ngOnInit() {
  }

  public menuItemClicked( iEvent, iMenuItemType: MenuItemType ) {
    this.onMenuItemClicked.emit( iMenuItemType );
    iEvent.preventDefault();
  }

  public goToProfile( iEvent ) {
    this.onMenuItemClicked.emit( MenuItemType.PROFILE );
    iEvent.preventDefault();
  }
}
