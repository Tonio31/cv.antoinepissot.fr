import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import { Pipe, PipeTransform } from '@angular/core';

describe( 'NavbarComponent', () => {

  @Pipe( { name: 'translate' } )
  class MockPipe implements PipeTransform {
    public transform( value: number ): number {
      return value;
    }
  }

  let component: NavbarComponent;
  let fixture: ComponentFixture<NavbarComponent>;

  beforeEach( async( () => {
    TestBed.configureTestingModule( {
      declarations: [
        NavbarComponent,
        MockPipe
      ]
    } )
    .compileComponents();
  } ) );

  beforeEach( () => {
    fixture = TestBed.createComponent( NavbarComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  } );

  it( 'should create', () => {
    expect( component ).toBeTruthy();
  } );
} );
