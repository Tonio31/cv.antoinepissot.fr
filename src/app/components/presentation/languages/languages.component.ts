import { Component, OnInit } from '@angular/core';
import { LogoType } from '@app/components/presentation/logo/logo.component';
import { LoggerService } from '@app/_services/logger.service';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss']
})
export class LanguagesComponent implements OnInit {

  private readonly MAX_RATING: number = 5;

  public languages: any = [
    {
      logoInfo: {
        type: LogoType.France,
        customCssClass: 'img-fluid'
      },
      rating: 5
    },
    {
      logoInfo: {
        type: LogoType.UK,
        customCssClass: 'img-fluid'
      },
      rating: 5
    },
    {
      logoInfo: {
        type: LogoType.Spain,
        customCssClass: 'img-fluid'
      },
      rating: 2
    },
  ];

  constructor(
    private logger: LoggerService, ) { }

  public ngOnInit() {
    this.logger.log( 'LanguagesComponent - ngOnInit()' );

    for ( const language of this.languages ) {
      language.stars = [];
      for ( let i = 1; i <= this.MAX_RATING; i++ ) {
        const typeStarIcon = i <= language.rating ? 'fas' : 'far';
        language.stars.push( typeStarIcon );
      }
    }
  }

}
