import { Component, OnInit } from '@angular/core';
import { environment } from '@src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { LogoSize, LogoType } from '@app/components/presentation/logo/logo.component';
import { Store } from '@ngrx/store';
import * as menuItem from '@app/reducers/menuItem/menuItem.reducer';
import { MenuItemClicked } from '@app/reducers/menuItem/menuItem.actions';
import { MenuItemType } from '@app/reducers/menuItem/menuItem.model';

@Component( {
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: [ './profile.component.scss' ]
} )
export class ProfileComponent implements OnInit {

  public readonly mePhotoUrlFromCdn: any = {
    medium: `${ environment.CDN_TONIO }images/tonio_headshot_medium.png`,
    large: `${ environment.CDN_TONIO }images/tonio_headshot_large.png`,
  };

  public readonly linkProfile = [
    {
      logoInfo: {
        type: LogoType.Linkedin,
        size: LogoSize.S
      },
      url: 'https://www.linkedin.com/in/antoinepissot'
    },
    {
      logoInfo: {
        type: LogoType.StackOverflow,
        size: LogoSize.SM
      },
      url: 'https://stackoverflow.com/users/4834117/tonio'
    },
  ];

  public aboutMe: string[] = [];

  constructor(
    private store: Store<menuItem.State>,
    private translate: TranslateService ) {
  }

  public ngOnInit() {

    this.translate.get('PROFILE.LIST_ABOUT_ME').subscribe((res: string[]) => {
      this.aboutMe = res;
    });
  }

  public goToContactForm() {
    this.store.dispatch( new MenuItemClicked( MenuItemType.CONTACT ) );
  }

}
