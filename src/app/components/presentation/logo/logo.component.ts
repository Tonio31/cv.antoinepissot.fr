import { Component, Input, OnInit } from '@angular/core';
import { LoggerService } from '@app/_services/logger.service';

export enum LogoType {
  Angular = 1,
  AngularJS,
  Agile,
  AWS,
  Bitbucket,
  Confluence,
  CPP,
  CSS,
  Docker,
  DynamoDB,
  Git,
  GitHub,
  Gulp,
  HTML,
  Jasmine,
  Javascript,
  Jira,
  Laravel,
  Linux,
  MacOS,
  MongoDB,
  PHP,
  PhpStorm,
  SCSS,
  SequelPro,
  SQL,
  TypeScript,
  UML,
  Webpack,

  Amadeus,
  INSA,
  Letspack,
  Linkedin,
  LeicesterUniversity,
  Potentialife,
  StackOverflow,

  Belize,
  Chile,
  Colombia,
  Ecuador,
  France,
  Guatemala,
  HongKong,
  India,
  Japan,
  Korea,
  Laos,
  Macau,
  Mexico,
  Myanmar,
  Panama,
  Peru,
  Singapore,
  Spain,
  Taiwan,
  Thailand,
  UK,
  US,
  Vietnam,

}

// Default value for max-height
export enum LogoSize {
  XS = 20,
  S = 40,
  SM = 60,
  M = 80,
  L = 160,
}

export enum LogoFolder {
  TECH = 1,
  COMPANIES,
  FLAG_COUNTRIES,
}

export interface LogoInfo {
  type: LogoType;
  customCssClass?: string;
  size?: LogoSize;
}


export interface LogoConfig {
  logoSrc: string;
  altImageText: string;
}

@Component( {
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: [ './logo.component.scss' ]
} )
export class LogoComponent implements OnInit {

  @Input() public logoInfo: LogoInfo;

  public logoConfig: LogoConfig = null;
  public logoStyle: any = null;
  public logoCustomCssClass: string = null;

  private readonly prefixLogo = {
    [ LogoFolder.TECH ]: `assets/images/logo-tech`,
    [ LogoFolder.COMPANIES ]: `assets/images/logo-companies`,
    [ LogoFolder.FLAG_COUNTRIES ]: `assets/images/flag-countries`,
  };

  private readonly defaultValueMaxHeight: number = LogoSize.M;
  private readonly defaultValueMaxWidth: number = this.defaultValueMaxHeight * 2;

  private readonly allLogosConfig = {
    [ LogoType.Angular ]: this.buildLogoConfig( 'angular.png' ),
    [ LogoType.AngularJS ]: this.buildLogoConfig( 'AngularJS.png' ),
    [ LogoType.Agile ]: this.buildLogoConfig( 'Agile.png' ),
    [ LogoType.AWS ]: this.buildLogoConfig( 'AWS.png' ),
    [ LogoType.Bitbucket ]: this.buildLogoConfig( 'bitbucket.png' ),
    [ LogoType.Confluence ]: this.buildLogoConfig( 'confluence.png' ),
    [ LogoType.CPP ]: this.buildLogoConfig( 'cpp.png' ),
    [ LogoType.CSS ]: this.buildLogoConfig( 'css.png' ),
    [ LogoType.Docker ]: this.buildLogoConfig( 'docker.png' ),
    [ LogoType.DynamoDB ]: this.buildLogoConfig( 'DynamoDB.png' ),
    [ LogoType.Git ]: this.buildLogoConfig( 'Git.png' ),
    [ LogoType.GitHub ]: this.buildLogoConfig( 'github.png' ),
    [ LogoType.Gulp ]: this.buildLogoConfig( 'gulp.png' ),
    [ LogoType.HTML ]: this.buildLogoConfig( 'HTML.png' ),
    [ LogoType.Jasmine ]: this.buildLogoConfig( 'JasmineJS.svg' ),
    [ LogoType.Javascript ]: this.buildLogoConfig( 'Javascript.png' ),
    [ LogoType.Jira ]: this.buildLogoConfig( 'Jira.png' ),
    [ LogoType.Laravel ]: this.buildLogoConfig( 'laravel.png' ),
    [ LogoType.Linux ]: this.buildLogoConfig( 'Linux.png' ),
    [ LogoType.MacOS ]: this.buildLogoConfig( 'macOS.png' ),
    [ LogoType.MongoDB ]: this.buildLogoConfig( 'mongoDB.png' ),
    [ LogoType.PHP ]: this.buildLogoConfig( 'PHP.svg' ),
    [ LogoType.PhpStorm ]: this.buildLogoConfig( 'PHP_Storm.png' ),
    [ LogoType.SCSS ]: this.buildLogoConfig( 'scss.png' ),
    [ LogoType.SequelPro ]: this.buildLogoConfig( 'sequel-pro.jpg' ),
    [ LogoType.SQL ]: this.buildLogoConfig( 'SQL.png' ),
    [ LogoType.TypeScript ]: this.buildLogoConfig( 'Typescript.png' ),
    [ LogoType.UML ]: this.buildLogoConfig( 'UML.png' ),
    [ LogoType.Webpack ]: this.buildLogoConfig( 'webpack.png' ),

    [ LogoType.Amadeus ]: this.buildLogoConfig( 'amadeus.jpg', LogoFolder.COMPANIES ),
    [ LogoType.INSA ]: this.buildLogoConfig( 'INSA.png', LogoFolder.COMPANIES ),
    [ LogoType.Letspack ]: this.buildLogoConfig( 'letspack.png', LogoFolder.COMPANIES ),
    [ LogoType.Linkedin ]: this.buildLogoConfig( 'linkedin.png', LogoFolder.COMPANIES ),
    [ LogoType.LeicesterUniversity ]: this.buildLogoConfig( 'Uni_leicester.png', LogoFolder.COMPANIES ),
    [ LogoType.Potentialife ]: this.buildLogoConfig( 'potentialife.svg', LogoFolder.COMPANIES ),
    [ LogoType.StackOverflow ]: this.buildLogoConfig( 'stack-overflow.png', LogoFolder.COMPANIES ),

    [ LogoType.Belize ]: this.buildLogoConfig( 'belize.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Chile ]: this.buildLogoConfig( 'chile.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Colombia ]: this.buildLogoConfig( 'colombia.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Ecuador ]: this.buildLogoConfig( 'ecuador.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.France ]: this.buildLogoConfig( 'france.svg', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Guatemala ]: this.buildLogoConfig( 'guatemala.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.HongKong ]: this.buildLogoConfig( 'hongkong.gif', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.India ]: this.buildLogoConfig( 'india.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Japan ]: this.buildLogoConfig( 'japan.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Korea ]: this.buildLogoConfig( 'korea.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Laos ]: this.buildLogoConfig( 'laos.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Macau ]: this.buildLogoConfig( 'macau.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Mexico ]: this.buildLogoConfig( 'mexico.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Myanmar ]: this.buildLogoConfig( 'myanmar.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Panama ]: this.buildLogoConfig( 'panama.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Peru ]: this.buildLogoConfig( 'peru.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Singapore ]: this.buildLogoConfig( 'singapore.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Spain ]: this.buildLogoConfig( 'spain.svg', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Taiwan ]: this.buildLogoConfig( 'taiwan.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Thailand ]: this.buildLogoConfig( 'thailand.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.UK ]: this.buildLogoConfig( 'united-kingdom.svg', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.US ]: this.buildLogoConfig( 'us.png', LogoFolder.FLAG_COUNTRIES ),
    [ LogoType.Vietnam ]: this.buildLogoConfig( 'vietnam.png', LogoFolder.FLAG_COUNTRIES ),
  };

  constructor( private logger: LoggerService ) {

  }

  public ngOnInit() {
    if ( this.logoInfo.type && this.allLogosConfig.hasOwnProperty( this.logoInfo.type ) ) {
      this.logoConfig = this.allLogosConfig[ this.logoInfo.type ];
    }
    else {
      this.logger.error( `LogoComponent - logoType(${ this.logoInfo.type }) is not defined within allLogos, no logo will be displayed` );
    }

    // Class is higher priority than size, we should not define a max-width if a class is defined as the
    // class might want to set this setting
    if ( this.logoInfo.customCssClass ) {
      this.logoCustomCssClass = this.logoInfo.customCssClass;
    }
    else {

      let maxWidth: number;
      let maxHeight: number;
      if ( this.logoInfo.size ) {
        maxHeight = this.logoInfo.size;
        maxWidth = maxHeight * 2;
      }
      else {
        maxWidth = this.defaultValueMaxWidth;
        maxHeight = this.defaultValueMaxHeight;
      }

      this.logoStyle = {
        'max-width.px': maxWidth,
        'max-height.px': maxHeight,
      };
    }

  }

  private buildLogoConfig( iLogoFileName: string, iFolderType: LogoFolder = LogoFolder.TECH ): LogoConfig {
    const prefix = this.prefixLogo[ iFolderType ];
    const logoSrc = `${ prefix }/${ iLogoFileName }`;
    const altImageText = iLogoFileName.substr(0, iLogoFileName.indexOf('.'));
    return {
      logoSrc,
      altImageText
    };
  }
}
