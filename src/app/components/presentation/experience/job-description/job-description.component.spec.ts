import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobDescriptionComponent } from './job-description.component';
import { CUSTOM_ELEMENTS_SCHEMA, Pipe, PipeTransform } from '@angular/core';
import { LogoSize, LogoType } from '@app/components/presentation/logo/logo.component';

describe('JobDescriptionComponent', () => {

  @Pipe( { name: 'translate' } )
  class MockTranslatePipe implements PipeTransform {
    public transform( value: number ): number {
      return value;
    }
  }

  @Pipe( { name: 'appSanitizeHtml' } )
  class MockSanitizeHtmlPipe implements PipeTransform {
    public transform( value: number ): number {
      return value;
    }
  }

  let component: JobDescriptionComponent;
  let fixture: ComponentFixture<JobDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        JobDescriptionComponent,
        MockTranslatePipe,
        MockSanitizeHtmlPipe
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDescriptionComponent);
    component = fixture.componentInstance;

    component.jobExperience = {
      company: {
        logo: {
          type: LogoType.Amadeus,
          customCssClass: 'img-fluid size-50'
        },
        url: 'https://amadeus.com/',
        name: 'Amadeus'
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42'
      },
      jobTitle: 'C++ Back-end Developer',
      dates: 'Jan 2010 - Dec 2013',
      jobSummary: 'Altea Customer Management & Flight Management',
      description: [
        `Responsible for designing, coding and delivering specified projects and modules in a multi-team and multi-sites (London-Sydney-Nice-Boston) environment`,
        `On-call duties for server side of a high availability, concurrent, mission-critical applications used 24/7 by more than 50 airlines across the globe`,
        `Improve the response time of DB queries by 30%`,
        `Development and deployment of python tools (web based) to improve developers’ efficiency`,
        `Design and implementation of a “back up” software that intercepts and duplicates all DB queries from Oracle to NoSql database`,
      ],
      logos: [
        {
          type: LogoType.AngularJS,
          size: LogoSize.S
        },
      ]
    };

    component.isSmallScreen = false;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
