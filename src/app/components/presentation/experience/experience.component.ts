import { Component, OnInit } from '@angular/core';
import { LogoInfo, LogoSize, LogoType } from '@app/components/presentation/logo/logo.component';


export interface SingleExperienceInterface {
  company: {
    logo: LogoInfo;
    url: string;
    name: string;
  };
  jobTitle: string;
  dates: string;
  jobSummary: string;
  description: string[];
  logos: LogoInfo[];
  location: {
    name: string;
    googleMapUrl: string;
  };
}

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  /* tslint:disable:max-line-length */
  public experiences: SingleExperienceInterface[] = [
    {
      company: {
        logo: {
          type: LogoType.Potentialife,
          customCssClass: 'img-fluid size-70'
        },
        url: 'https://potentialife.com',
        name: 'Potentialife'
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42'
      },
      jobTitle: 'Chief Technology Officer',
      dates: 'Jan 2018 - current',
      jobSummary: 'Potentialife transforms organisations by changing behaviours at scale. Using our technology platform and behavioural-science programmes, we help people become happier and more productive leaders. Participants login to a website (comparable to udacity) where they watch videos, do exercises, answer survey…',
      description: [
        `<strong>Recruiting & managing the tech team (5 people)</strong>, supporting their personal development to build on their skills (quarterly review, bi-weekly one-on-one) to ensure that the tech department <strong>follows best practice</strong> in code writing / documentation / unit test`,
        `Owning the technical responsibility of <strong>all front-end</strong> development`,
        `<strong>Setting up Agile methodology</strong> (daily stand-up, weekly sprint planning, monthly retrospective to stakeholders) resulting in 50% increase in projects delivery and more accurate delivery dates`,
        `<strong>Organising backlog</strong> & creating user stories based on monthly meeting with stakeholders`,
        `Leading the implementation of <strong>GDPR</strong>`,
      ],
      logos: [
        {
          type: LogoType.Angular,
          size: LogoSize.S
        },
        {
          type: LogoType.TypeScript,
          size: LogoSize.S
        },
        {
          type: LogoType.AngularJS,
          size: LogoSize.S
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS
        },
        {
          type: LogoType.HTML,
          size: LogoSize.S
        },
        {
          type: LogoType.CSS,
          size: LogoSize.S
        },
        {
          type: LogoType.PHP,
          size: LogoSize.XS
        },
        {
          type: LogoType.Laravel,
          size: LogoSize.S
        },
        {
          type: LogoType.AWS,
          size: LogoSize.S
        },
        {
          type: LogoType.Docker,
          size: LogoSize.S
        },
      ]
    },
    {
      company: {
        logo: {
          type: LogoType.Potentialife,
          customCssClass: 'img-fluid size-70'
        },
        url: 'https://potentialife.com',
        name: 'Potentialife'
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42'
      },
      jobTitle: 'Lead Front-end Developer',
      dates: 'Jan 2017 - Dec 2017',
      jobSummary: 'I created  two websites, one used by participants (Angularjs) and the other to manage content and participants, and enable autonomous use by all internal stakeholders  (Angular).',
      description: [
        `Owning the technical responsibility of <strong>all front-end</strong> development`,
        `<strong>Managing 2 interns & 3 contractors</strong> making sure they follow code writing guidelines (eslint/tslint) / unit test (karma + jasmine)`,
        `Managing web hosting on S3 / CloudFront / Route53 within AWS`,
      ],
      logos: [
        {
          type: LogoType.Angular,
          size: LogoSize.S
        },
        {
          type: LogoType.TypeScript,
          size: LogoSize.S
        },
        {
          type: LogoType.AngularJS,
          size: LogoSize.S
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS
        },
        {
          type: LogoType.PHP,
          size: LogoSize.XS
        },
        {
          type: LogoType.Laravel,
          size: LogoSize.S
        },
        {
          type: LogoType.AWS,
          size: LogoSize.S
        },
        {
          type: LogoType.Webpack,
          size: LogoSize.S
        },
        {
          type: LogoType.Docker,
          size: LogoSize.S
        }
      ]
    },
    {
      company: {
        logo: {
          type: LogoType.Amadeus,
          customCssClass: 'img-fluid size-50'
        },
        url: 'https://amadeus.com/',
        name: 'Amadeus'
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42'
      },
      jobTitle: 'AngularJs Developer',
      dates: 'Jul 2015 - Dec 2016',
      jobSummary: 'Altea Customer Management & Flight Management - Amadeus is the world leading solution to automate key airport processes for airlines from check-in to departure. As the Technical project leader, I led the delivery of new flight history display website (based on Angularjs)',
      description: [
        `Maintaining the backlog / Creating User stories`,
        `Setting up working environment using gulp / git / jasmine /eslint`,
        `Working in a full agile environment with regular customer engagement`,
      ],
      logos: [
        {
          type: LogoType.AngularJS,
          size: LogoSize.S
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS
        },
        {
          type: LogoType.HTML,
          size: LogoSize.S
        },
        {
          type: LogoType.CSS,
          size: LogoSize.S
        },
        {
          type: LogoType.MongoDB,
          size: LogoSize.S
        },
        {
          type: LogoType.Git,
          size: LogoSize.XS
        },
        {
          type: LogoType.Gulp,
          size: LogoSize.S
        },
        {
          type: LogoType.Jasmine,
          size: LogoSize.S
        }
      ]
    },
    {
      company: {
        logo: {
          type: LogoType.Letspack,
          customCssClass: 'img-fluid'
        },
        url: null,
        name: 'Letspack'
      },
      location: {
        name: 'World',
        googleMapUrl: 'https://earth.google.com/web/'
      },
      jobTitle: 'Sabbatical Year',
      dates: 'Jan 2014 - Dec 2014',
      jobSummary: 'I took a sabbatical year to backpack around the world, from South America to Thailand via United States and Japan. I travelled through 18 countries.',
      description: [],
      logos: [
        {
          type: LogoType.Chile,
          size: LogoSize.S
        },
        {
          type: LogoType.Peru,
          size: LogoSize.S
        },
        {
          type: LogoType.Ecuador,
          size: LogoSize.S
        },
        {
          type: LogoType.Colombia,
          size: LogoSize.S
        },
        {
          type: LogoType.Panama,
          size: LogoSize.S
        },
        {
          type: LogoType.Guatemala,
          size: LogoSize.S
        },
        {
          type: LogoType.Belize,
          size: LogoSize.S
        },
        {
          type: LogoType.Mexico,
          size: LogoSize.S
        },
        {
          type: LogoType.US,
          size: LogoSize.S
        },
        {
          type: LogoType.Japan,
          size: LogoSize.S
        },
        {
          type: LogoType.Taiwan,
          size: LogoSize.S
        },
        {
          type: LogoType.HongKong,
          size: LogoSize.S
        },
        {
          type: LogoType.Macau,
          size: LogoSize.S
        },
        {
          type: LogoType.Vietnam,
          size: LogoSize.S
        },
        {
          type: LogoType.Laos,
          size: LogoSize.S
        },
        {
          type: LogoType.Myanmar,
          size: LogoSize.S
        },
        {
          type: LogoType.Singapore,
          size: LogoSize.S
        },
        {
          type: LogoType.Thailand,
          size: LogoSize.S
        }
      ]
    },
    {
      company: {
        logo: {
          type: LogoType.Amadeus,
          customCssClass: 'img-fluid size-50'
        },
        url: 'https://amadeus.com/',
        name: 'Amadeus'
      },
      location: {
        name: 'London',
        googleMapUrl: 'https://goo.gl/maps/izggaxoNfe42'
      },
      jobTitle: 'C++ Back-end Developer',
      dates: 'Jan 2010 - Dec 2013',
      jobSummary: 'Altea Customer Management & Flight Management - Amadeus is the world leading solution to automate key airport processes for airlines from check-in to departure. As the Technical project leader, I led the delivery of new flight history display website (based on Angularjs)',
      description: [
        `Responsible for designing, coding and delivering specified projects and modules in a multi-team and multi-sites (London-Sydney-Nice-Boston) environment`,
        `On-call duties for server side of a high availability, concurrent, mission-critical applications used 24/7 by more than 50 airlines across the globe`,
        `Improve the response time of DB queries by 30%`,
        `Development and deployment of python tools (web based) to improve developers’ efficiency`,
        `Design and implementation of a “back up” software that intercepts and duplicates all DB queries from Oracle to NoSql database`,
      ],
      logos: [
        {
          type: LogoType.AngularJS,
          size: LogoSize.S
        },
        {
          type: LogoType.Javascript,
          size: LogoSize.XS
        },
        {
          type: LogoType.CPP,
          size: LogoSize.XS
        },
        {
          type: LogoType.MongoDB,
          size: LogoSize.S
        },
        {
          type: LogoType.Git,
          size: LogoSize.XS
        },
        {
          type: LogoType.Gulp,
          size: LogoSize.S
        },
        {
          type: LogoType.Jasmine,
          size: LogoSize.S
        }
      ]
    },
  ];

  constructor() { }

  public ngOnInit() {
  }


}
