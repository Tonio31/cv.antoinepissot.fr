import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LogoSize, LogoType } from '@app/components/presentation/logo/logo.component';
// import 'bootstrap';

@Component({
  selector: 'app-interests',
  templateUrl: './interests.component.html',
  styleUrls: ['./interests.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InterestsComponent implements OnInit {

  public selectedTab: number;

  public listInterests = [
    {
      iconClass: 'fas fa-utensils',
      content: {
        image: 'assets/images/tartiflette.jpg',
        title: 'Cooking',
        body: [
          'I love organising dinner party and cook for my friends',
          'My best recipe is probably the tartiflette (oven potatoes with reblochon)'
        ]
      }
    },
    {
      iconClass: 'fas fa-hiking',
      content: {
        image: 'assets/images/bagan.jpg',
        title: 'Trekking & Travelling',
        body: [
          `I feel happy when I'm struggling to reach a top of a mountain`,
          `The trek I'm most proud of is the GR20: crossing Corsica in 10 days (200km, 14km elevation)` ,
          `I want to visit 100 countries in my life (I'm at 38)`,
        ]
      }
    },
    {
      iconClass: 'fas fa-bicycle',
      content: {
        image: 'assets/images/cycling-brighton.jpg',
        title: 'Cycling',
        body: [
          `I'm saving a lot of money by always cycling and never taking the tube`,
          `I cycled from Paris to London in 2018, it was awesome`,
          `My biggest race was Prudential London in 2018 - 100 miles`,
        ]
      }
    },
    {
      iconClass: 'fas fa-futbol',
      content: {
        image: null,
        title: 'Football',
        body: [
          `Part of a 5-side team in a competitive league in west London`,
          `I'm not really good but I love playing this sport`,
        ]
      }
    },
    {
      iconClass: 'fas fa-volleyball-ball',
      content: {
        image: null,
        title: 'Volley-Ball',
        body: [
          `I played 10 years when I was younger`,
          `Best achievement is winning the French University Championship in 2006 & 2007`,
        ]
      }
    },
  ];



  constructor() { }

  ngOnInit() {
  }

  public mouseEnter( iTabToSelect ) {
    this.selectedTab = iTabToSelect;
  }

}
