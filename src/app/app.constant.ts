import { InjectionToken } from '@angular/core';

export interface IAppConstant {
  ENV: {
    LOGO_URL: string;
  };
}

export const APP_DI_CONSTANT: IAppConstant = {
  ENV: {
    LOGO_URL: 'https://cdn.potentialife.com/assets/company_logos/',
  },
};

export let APP_CONSTANT = new InjectionToken<IAppConstant>( 'app.constant' );
