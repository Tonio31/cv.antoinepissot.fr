import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpService } from '@app/_services/http.service';
import { of, throwError } from 'rxjs';
import { LoggerService } from '@app/_services/logger.service';
import { MockLoggerService } from '@app/_services/logger.service.spec';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import { StorageService } from '@app/_services/storage.service';
import { AnalyticsService } from '@app/_services/analytics.service';
import { CookieService } from '@app/_services/cookie.service';
import { TranslateService } from '@ngx-translate/core';


describe( 'AppComponent', () => {


  class MockTranslateService {
    public setDefaultLang() {
    }

    public use() {}
  }

  class MockAnalyticsService {
    public init() {
    }
  }

  class MockCookieService {
    public init() {
    }
  }

  beforeEach( async( () => {
    TestBed.configureTestingModule( {
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: TranslateService, useClass: MockTranslateService },
        { provide: AnalyticsService, useClass: MockAnalyticsService },
        { provide: CookieService, useClass: MockCookieService },
        { provide: LoggerService, useClass: MockLoggerService },
      ],
      declarations: [
        AppComponent
      ],
    } ).compileComponents();
  } ) );

  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  let translate: TranslateService;
  let analytics: AnalyticsService;
  let cookieService: CookieService;

  beforeEach( () => {
    fixture = TestBed.createComponent( AppComponent );
    comp = fixture.componentInstance;

    translate = fixture.debugElement.injector.get( TranslateService );
    analytics = fixture.debugElement.injector.get( AnalyticsService );
    cookieService = fixture.debugElement.injector.get( CookieService );

    jasmine.clock().uninstall();
    jasmine.clock().install();

    const baseTimeUTC = new Date( Date.UTC( 2017, 8, 7 ) );
    jasmine.clock().mockDate( baseTimeUTC );

  } );

  afterEach( () => {
    jasmine.clock().uninstall();
  } );

  it( 'ngOnInit()', () => {
    spyOn( translate, 'setDefaultLang' );
    spyOn( translate, 'use' );
    spyOn( analytics, 'init' );
    spyOn( cookieService, 'init' );

    fixture.detectChanges();  // calls ngOnInit()

    expect( translate.setDefaultLang ).toHaveBeenCalledWith( 'en' );
    expect( translate.use ).toHaveBeenCalledWith( 'en' );
    expect( analytics.init ).toHaveBeenCalled();
    expect( cookieService.init ).toHaveBeenCalled();
  } );

} );
