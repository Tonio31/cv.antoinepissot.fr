import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable( {
  providedIn: 'root'
} )
export class AppMockBackendService implements HttpInterceptor {

  private error500: HttpErrorResponse;

  public intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<any> {

    this.error500 = new HttpErrorResponse( {
      status: 500,
      statusText: 'System Error',
      error: {
        message: 'System Error',
      },
    } );

    // Uncomment this to stop intercepting request
    // return next.handle( request );

    // public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const url: string = request.urlWithParams;
    const method: string = request.method;

    console.warn( `AppMockBackEnd - url=${ url },   method=${ method }` );

    // This is here as without it, we can't see in the console the headers, they are there but lazy-loaded
    // I don't really understand how but if you want to debug and see the headers in the console, you will
    // only be able to see them after this line of code
    request.headers.keys();

    if ( request.url.endsWith( 'send-message' ) && request.method === 'POST' ) {

      // return throwError( this.error500 );
      return new Observable( ( resp ) => {
        resp.next( new HttpResponse( {
          status: 200,
          body: {
            status: 'OK',
          },
        } ) );
        resp.complete();
      } );
    }

    // All unhandled request goes through
    return next.handle( request );
  }

}
