import * as MenuItemsActions from './menuItem.actions';
import { MenuItemType } from '@app/reducers/menuItem/menuItem.model';

export interface State {
  currentMenuItem: MenuItemType;
}

export const initialState: State = {
  currentMenuItem: MenuItemType.PROFILE,
};

export function menuItemReducer( state = initialState, action: MenuItemsActions.Actions ) {
  switch ( action.type ) {
    case MenuItemsActions.ActionTypes.MenuItemClicked: {
      return {
        ...state,
        currentMenuItem: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

export const getCurrentMenuItem = (state: State) => {
  if ( state ) {
    return state.currentMenuItem;
  }

  console.error( 'getCurrentMenuItem() - State is not defined' );
  return initialState.currentMenuItem;
};
