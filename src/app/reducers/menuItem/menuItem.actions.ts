import { Action } from '@ngrx/store';


import {
  MenuItem, MenuItemType
} from './menuItem.model';

export enum ActionTypes {
  MenuItemClicked = '[MenuItem] Set Clicked',
}

export class MenuItemClicked implements Action {
  readonly type = ActionTypes.MenuItemClicked;

  constructor( public payload: MenuItemType ) {
  }
}

export type Actions = MenuItemClicked;
