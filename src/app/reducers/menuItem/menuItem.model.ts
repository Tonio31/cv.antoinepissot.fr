/**
 * A User represents an agent that sends messages
 */

export enum MenuItemType {
  PROFILE = 1,
  EXPERTISE,
  EXPERIENCE,
  EDUCATION,
  LANGUAGES,
  INTERESTS,
  CONTACT,
}

export interface MenuItem {
  category: MenuItemType;
}
