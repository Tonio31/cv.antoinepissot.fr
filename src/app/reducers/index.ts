import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '@src/environments/environment';

import * as menuItem from './menuItem/menuItem.reducer';

// console.log all actions
export function logger(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}

export interface State {
  menuItem: menuItem.State;
}

export const reducers: ActionReducerMap<State> = {
  menuItem: menuItem.menuItemReducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [ logger ] : [];

export const getMenuItemState = createFeatureSelector<menuItem.State>('menuItem');


export const getMenuItemType = createSelector(
  getMenuItemState,
  menuItem.getCurrentMenuItem
);
