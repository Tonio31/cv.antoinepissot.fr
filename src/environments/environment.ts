// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppMockBackendService } from '@app/app.mock-backend.service';
import bugsnag from '@bugsnag/js';

export const environment = {
  production: false,
  DOMAIN: 'localhost',
  CDN_TONIO: 'https://cdn.antoinepissot.fr/',
  BACKEND_API: 'http://localhost:3333/',
  BUGSNAG_KEY: '420acbd676c793e34e9a858b90c7b3ad',
  googleAnalyticsKey: 'UA-138509225-1',
  VERSION: require('../../package.json').version,
  TARGET_DEPLOYMENT: 'development',
};

export const bugsnagClient = bugsnag( {
  apiKey: environment.BUGSNAG_KEY,
  appVersion: environment.VERSION,
  releaseStage: environment.TARGET_DEPLOYMENT,
  notifyReleaseStages: [ 'PROD' ],
  autoCaptureSessions: true,
} );

export const ENV_PROVIDERS = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AppMockBackendService,
    multi: true,
  },
];

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
